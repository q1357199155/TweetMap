# 在线实时全球推特话题热度趋势图

1. 通过集成Twitter Streaming API，用`java`构建实时推特收集程序
2. 将获取到的实时推文发送到`Kafka`集群中
3. 利用`Apache Flink`进行实时推文流处理（解析、过滤、格式转换）
4. 将处理过的推文存储到`Elasticsearch`以便数据持久化和索引
5. 利用`SpringBoot`开发`RESTful API`并从`Elasticsearch`中获取推文
6. 搭建一个基于`Angular`的前端应用程序可视化实时推特热度话题分布图
7. 使用`Kibana`显示推特地理热度图
8. 构建`Docker` image并部署各微服务组件到`Kubernetes`集群

![](TweetMap-Architecture.png)

![](ScaledCircle.png)
![](HeatMap.png)


## Build

./gradlew :tweet-collector:clean :tweet-collector:build :tweet-collector:dockerPush

java -jar tweet-collector/build/libs/tweet-collector-1.1-SNAPSHOT.jar

./gradlew :flink-processor:clean :flink-processor:build


# Tweet Map with Trends

1. Build a `java` Tweet Collector to collect real-time tweets with Twitter Streaming API
2. Push collected tweets to `Kafka` cluster
3. Utilize `Apache Flink` Streaming to process (parse, filter and tranform) tweets
4. Ingest processed tweets to `Elasticsearch` for data persistance and index
5. Develop `SpringBoot` `RESTful API server` to query tweets from Elasticsearch 
6. Visualize real time tweet trends with Frontend `Angular` web application
7. Show Geographical Tweet Heat Map with `Kibana`
8. Create `Docker` image and deploy `microservices` to `Kubernetes` cluster
